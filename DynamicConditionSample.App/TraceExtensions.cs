﻿namespace DynamicConditionSample.App
{
    using System;
    using System.Data;
    using System.Diagnostics;

    public static class TraceExtensions
    {
        public static void ExecuteAndTrace(this Action subject, string name)
        {
            var sw = Stopwatch.StartNew();
            try
            {
                subject();
            }
            finally
            {
                sw.Stop();
                Show(name, sw.Elapsed);
                
            }
        }

        public static T ExecuteAndTrace<T>(this Func<T> subject, string name)
        {
            var sw = Stopwatch.StartNew();
            try
            {
                return subject();
            }
            finally
            {
                sw.Stop();
                Show(name, sw.Elapsed);
            }
        }

        static void Show(string name, TimeSpan elapsed)
        {
            var color = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(name + " took: " + elapsed);
            Console.ForegroundColor = color;
        }
    }
}