﻿using System;
using System.Collections.Generic;
using System.Linq;
using DynamicConditionSample.Context;
using MongoDB.Bson;
using Newtonsoft.Json.Linq;

namespace DynamicConditionSample.App
{
    using Engine;

    class Program
    {
        [LoaderOptimization(LoaderOptimization.MultiDomainHost)]
        static void Main(string[] args)
        {

            var context = new ConditionContext()
            {
                Request = new Request
                {
                    Uri = new Uri("http://api.musicland.com/tracks/id"),
                    Headers = new Dictionary<string, string>
                    {
                        { "Accept", "audio/mpeg" },
                        { "Via", "shazam.com" }
                    },
                },
                Caller = new User
                {
                    Email = "a@b.com",
                    Groups = new[] 
                    {
                        new Group { Name = "admins" },
                        new Group { Name = "vips" }
                    }
                }
            };

            Console.WriteLine("Sample context state: ");
            Console.WriteLine(JObject.FromObject(context));

            IConditionValidator validator = new ConditionEvaluatorBuilder();
            using (var conditionRepo = new ConditionRepository())
            {
                string input;
                Console.Write("Enter condition or 'exit': ");
                while ((input = Console.ReadLine().Trim()).ToLower() != "exit")
                {
                    try
                    {
                        TraceExtensions.ExecuteAndTrace(() => validator.ValidateAndThrow(input), "validate");
                        
                        var key = "Condition_" + Id();
                        TraceExtensions.ExecuteAndTrace(() => conditionRepo.ReInitialize(new Dictionary<string, string>
                        {
                            { key, input }
                        }), "reload");
                        
                        var condition = conditionRepo.Get(key);
                        Console.WriteLine("Evaluation result: " + condition(context));
                    }
                    catch (ConditionParsingException ex)
                    {
                        Console.WriteLine("Syntax error:");
                        Console.WriteLine(ex.Message);
                    }
                    finally
                    {
                        Console.Write("Enter condition or 'exit': ");
                    }
                }
            }
        }

        static string Id()
        {
            return ObjectId.GenerateNewId().ToString();
        }
    }
}
