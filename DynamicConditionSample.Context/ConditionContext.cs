﻿namespace DynamicConditionSample.Context
{
    public class ConditionContext
    {
        public Request Request { get; set; }

        public User Caller { get; set; }
    }
}