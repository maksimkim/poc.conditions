﻿namespace DynamicConditionSample.Context
{
    using System;
    using System.Collections.Generic;

    public class Request
    {
        public Dictionary<string, string> Headers { get; set; }
        public Uri Uri { get; set; }
    }
}