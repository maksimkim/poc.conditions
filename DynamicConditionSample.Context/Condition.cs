﻿namespace DynamicConditionSample.Context
{
    public delegate bool Condition(ConditionContext context);
}
