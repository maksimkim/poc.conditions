﻿namespace DynamicConditionSample.Context
{
    using System.Collections.Generic;

    public class User
    {
        public string Email { get; set; }
        public IEnumerable<Group> Groups { get; set; }
    }
}