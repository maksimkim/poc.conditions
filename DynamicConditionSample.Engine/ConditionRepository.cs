﻿namespace DynamicConditionSample.Engine
{
    using System;
    using System.Collections.Generic;
    using System.Runtime;
    using System.Runtime.InteropServices;
    using Context;
    using MongoDB.Bson;

    public class ConditionRepository : IConditionRepository
    {
        class ConditionGateway : MarshalByRefObject
        {
            public void Populate(IntPtr inputRef, IntPtr onReadyRef)
            {
                var input = (IDictionary<string, string>)GCHandle.FromIntPtr(inputRef).Target;
                var onReady = (Action<IDictionary<string, Condition>>)GCHandle.FromIntPtr(onReadyRef).Target;
                var conditions = new ConditionEvaluatorBuilder().Build(input);
                onReady(conditions);
            }
        }

        AppDomain conditionDomain;

        IDictionary<string, Condition> conditions;

        public ConditionRepository()
            : this(null)
        {
            
        }
        
        public ConditionRepository(IDictionary<string, string> conditions)
        {
            this.ReInitialize(conditions);
        }

        public Condition Get(string key)
        {
            if (this.conditions == null)
            {
                return null;
            }
            Condition result;
            this.conditions.TryGetValue(key, out result);
            return result;
        }

        public void ReInitialize(IDictionary<string, string> conditions)
        {
            this.CleanUp();

            if (conditions == null || conditions.Count == 0)
            {
                return;
            }

            this.conditionDomain = AppDomain.CreateDomain(
                "ConditionDomain." + Id(),
                AppDomain.CurrentDomain.Evidence,
                new AppDomainSetup { LoaderOptimization = LoaderOptimization.MultiDomainHost }
            );

            var gateway = (ConditionGateway)this.conditionDomain.CreateInstanceAndUnwrap(
                typeof(ConditionGateway).Assembly.FullName,
                typeof(ConditionGateway).FullName
            );

            this.Wire(gateway, conditions);
        }

        public void Dispose()
        {
            this.CleanUp();
        }

        void Wire(ConditionGateway gateway, IDictionary<string, string> conditions)
        {
            var old = GCSettings.LatencyMode;
            try
            {
                GCSettings.LatencyMode = GCLatencyMode.Batch; // try to keep the GC out of our stuff
                
                var conditionsRef = GCHandle.ToIntPtr(GCHandle.Alloc(conditions));
                var setterRef = GCHandle.ToIntPtr(GCHandle.Alloc(new Action<IDictionary<string, Condition>>(res => this.conditions = res)));
                gateway.Populate(conditionsRef, setterRef);
            }
            finally
            {
                GCSettings.LatencyMode = old;
            }
        }

        void CleanUp()
        {
            //todo: thread safety (interlocked+spinwait)
            this.conditions = null;
            if (this.conditionDomain != null)
            {
                AppDomain.Unload(this.conditionDomain);
            }
        }

        static string Id()
        {
            return ObjectId.GenerateNewId().ToString();
        }
    }
}