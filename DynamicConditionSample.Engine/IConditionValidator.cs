namespace DynamicConditionSample.Engine
{
    public interface IConditionValidator
    {
        void ValidateAndThrow(string condition);
    }
}