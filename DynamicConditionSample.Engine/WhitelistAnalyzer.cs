﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading;
using DynamicConditionSample.Context;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.Diagnostics;

namespace DynamicConditionSample.Engine
{
    public class WhitelistAnalyzer : ISyntaxNodeAnalyzer<SyntaxKind>
    {
        const string ConditionCategory = "Microsoft.Azure.ApiManagement.Policy.Conditions";

        private static readonly DiagnosticDescriptor TypeUsageDiag
            = new DiagnosticDescriptor("APIM0001", "Type usage not supported", "Type '{0}' usage not supported", ConditionCategory, DiagnosticSeverity.Error, true);

        private static readonly DiagnosticDescriptor MethodUsageDiag
            = new DiagnosticDescriptor("APIM0002", "Method usage not supported", "Member '{0}' usage of type '{1}' not supported", ConditionCategory, DiagnosticSeverity.Error, true);

        static readonly ImmutableArray<DiagnosticDescriptor> supportedDiagnostics = ImmutableArray.Create(TypeUsageDiag, MethodUsageDiag);

        readonly static IDictionary<string, string[]> MemberWhitelist = new Dictionary<string, string[]>
        {
            { "IDictionary", new[] { "Item", "ContainsKey" } },
            { "Dictionary", new[] { "Item", "ContainsKey" } },
            { "Enumerable", new[] { "Contains", "Any" } },
            { "String", new[] { "StartsWith", "Length" } },
            { "DateTime", new[] { "Day", "UtcNow" } },
        };

        private static readonly string[] TrustedAssemblyNames =
        {
            typeof(ConditionContext).Assembly.GetName().Name
        };

        private static readonly ImmutableArray<SyntaxKind> InterestedKinds = ImmutableArray.Create(
            SyntaxKind.InvocationExpression,
            SyntaxKind.SimpleMemberAccessExpression,
            SyntaxKind.ElementAccessExpression
        );

        public ImmutableArray<DiagnosticDescriptor> SupportedDiagnostics
        {
            get { return supportedDiagnostics; }
        }

        public ImmutableArray<SyntaxKind> SyntaxKindsOfInterest
        {
            get { return InterestedKinds; }
        }

        public void AnalyzeNode(SyntaxNode node, SemanticModel semanticModel, Action<Diagnostic> addDiagnostic, AnalyzerOptions options, CancellationToken cancellationToken)
        {
            Diagnostic error = null;
            switch ((SyntaxKind)node.RawKind)
            {
                case SyntaxKind.InvocationExpression:
                    error = ValidateMember(semanticModel, node);
                    break;
                case SyntaxKind.SimpleMemberAccessExpression:
                case SyntaxKind.ElementAccessExpression:
                    error = ValidateMember(semanticModel, node);
                    break;
            }

            if (error != null)
            {
                addDiagnostic(error);
            }
        }

        static Diagnostic ValidateMember(SemanticModel model, SyntaxNode node)
        {
            var symbolInfo = model.GetSymbolInfo(node);
            var memberSymbol = symbolInfo.Symbol;

            if (memberSymbol == null)
            {
                return null;
            }

            if (TrustedAssemblyNames.Contains(memberSymbol.ContainingAssembly.Name))
            {
                return null;
            }
            
            var typeSymbol = memberSymbol.ContainingSymbol;
            
            var typeName = typeSymbol.Name;
            if (!MemberWhitelist.ContainsKey(typeName))
            {
                return Diagnostic.Create(TypeUsageDiag, node.GetLocation(), typeSymbol.ToDisplayString());
            }

            var methodName = memberSymbol.MetadataName;
            if (!MemberWhitelist[typeName].Contains(methodName))
            {
                return Diagnostic.Create(MethodUsageDiag, node.GetLocation(), memberSymbol.Name, typeSymbol.ToDisplayString());
            }

            return null;
        }
    }
}