﻿namespace DynamicConditionSample.Engine
{
    using System;
    using System.Collections.Generic;
    using DynamicConditionSample.Context;

    public interface IConditionRepository : IDisposable
    {
        Condition Get(string key);

        void ReInitialize(IDictionary<string, string> conditions);
    }
}