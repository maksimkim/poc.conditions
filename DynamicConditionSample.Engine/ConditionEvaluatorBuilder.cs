﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using DynamicConditionSample.Context;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.Diagnostics;
using MongoDB.Bson;

namespace DynamicConditionSample.Engine
{
    public class ConditionEvaluatorBuilder : IConditionValidator
    {
        const string ContainerTypeName = "ConditionContainer";

        const string ConditionClassTemplate =
            @"
            using System;
            using System.Collections.Generic;
            using System.Linq;
            using System.Text;
            using DynamicConditionSample.Context;
        
            namespace {0}
            {{
                public static class " + ContainerTypeName + @"
                {{
                    {1}
                }}
            }}";

        const string ConditionMethodTemplate =
            @"public static bool {0}(ConditionContext ctx)
            {{
                return {1};
            }}
            ";

        public IDictionary<string, Condition> Build(IDictionary<string, string> conditions)
        {
            var compilation = CreateCompilation(conditions);

            Assembly asm;
            using (var stream = new MemoryStream())
            {
                Compile(compilation, stream);
                asm = Assembly.Load(stream.GetBuffer());
            }

            var type = asm.GetTypes().Single();

            var result = type
                .GetMethods(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Static)
                .ToDictionary(
                    method => method.Name,
                    method => new Condition((Func<ConditionContext, bool>)Delegate.CreateDelegate(typeof(Func<ConditionContext, bool>), method, true))
                );

            return result;
        }

        public void ValidateAndThrow(string condition)
        {
            var compilation = CreateCompilation(new KeyValuePair<string, string>("Conditiond_" + Id(), condition));

            IEnumerable<Diagnostic> errors = compilation.GetDiagnostics(CancellationToken.None).Where(d => d.Severity == DiagnosticSeverity.Error);
            if (errors.Any())
            {
                throw new ConditionParsingException(string.Join("\n", errors));
            }

            //todo: map line number to condition 
            errors = AnalyzerDriver.GetDiagnostics(
                compilation,
                new[] { (IDiagnosticAnalyzer)new WhitelistAnalyzer() },
                null,
                CancellationToken.None
            );
            if (errors.Any())
            {
                throw new ConditionParsingException(string.Join("\n", errors));
            }
        }

        static void Compile(Compilation compilation, MemoryStream stream)
        {
            var emitResult = compilation.Emit(stream);
            if (!emitResult.Success)
            {
                throw new ConditionParsingException(string.Join("\n", emitResult.Diagnostics));
            }
        }

        static Compilation CreateCompilation(IEnumerable<KeyValuePair<string, string>> conditions)
        {
            return CreateCompilation(conditions.ToArray());
        }

        static Compilation CreateCompilation(params KeyValuePair<string, string>[] conditions)
        {
            var moduleName = "Conditions" + Id();

            var source = string.Format(
                ConditionClassTemplate,
                moduleName, //namespace
                string.Join(string.Empty, conditions.Select(pair => string.Format(ConditionMethodTemplate, pair.Key, pair.Value))) //methods
            );


            var compilation = CSharpCompilation
                .Create(moduleName, options: new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary, debugInformationKind: DebugInformationKind.PdbOnly))
                .AddReferences(new MetadataFileReference(typeof(object).Assembly.Location))
                .AddReferences(new MetadataFileReference(typeof(Enumerable).Assembly.Location))
                .AddReferences(new MetadataFileReference(typeof(ConditionContext).Assembly.Location))
                .AddSyntaxTrees(CSharpSyntaxTree.ParseText(source))
            ;

            return compilation;
        }

        static string Id()
        {
            return ObjectId.GenerateNewId().ToString();
        }
    }
}
