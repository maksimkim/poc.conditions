﻿using System;

namespace DynamicConditionSample.Engine
{
    public class ConditionParsingException : Exception
    {
        public ConditionParsingException(string message)
            : base(message)
        {
            
        }
    }
}